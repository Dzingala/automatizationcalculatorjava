package by.bsu.calculator;


public class Calculator {
    private double firstNumber;
    private double secondNumber;
    private char operation;

    public Calculator() {
        //Default constructor
    }

    public Calculator(double firstNumber, double secondNumber, char operation) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.operation = operation;
    }

    public double getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public char getOperation() {
        return operation;
    }

    public void setOperation(char operation) {
        this.operation = operation;
    }

    /**
     * This method sets corresponding fields of an existing calculator object from an expression come.
     * @param input expression come as a whole string.
     */
    public void setData(String input){
        String reg="^[-+]?[0-9]{0,}([-+*/]?)[0-9]+$";
        if(input.matches(reg)){
            String[] symbols;
            symbols=input.split("(?<=[-+*/])|(?=[-+*/])");
            firstNumber=Double.valueOf(symbols[0]);
            operation=symbols[1].charAt(0);
            secondNumber=Double.valueOf(symbols[2]);
        }else{
            System.out.println("Incorrect input. Please, restart an application and try again");
        }
    }
}
