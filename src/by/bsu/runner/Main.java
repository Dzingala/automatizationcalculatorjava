package by.bsu.runner;

import by.bsu.calculator.Calculator;
import by.bsu.util.OperationsPerformer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the expression in format a+b");
        String expression = br.readLine();
        Calculator calculator= new Calculator();
        calculator.setData(expression);
        double result = OperationsPerformer.calculate(calculator);
        System.out.println("="+result);
    }

}
