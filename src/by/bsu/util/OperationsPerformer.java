package by.bsu.util;


import by.bsu.calculator.Calculator;

public class OperationsPerformer {

    public static double calculate(Calculator calculator){
        String operation=""+calculator.getOperation();
        double result;
        switch (operation){
            case "+":{
                result= calculator.getFirstNumber()+calculator.getSecondNumber();
                break;
            }
            case "*":{
                result= calculator.getFirstNumber()*calculator.getSecondNumber();
                break;
            }
            case "/":{
                result= calculator.getFirstNumber()/calculator.getSecondNumber();
                break;
            }
            case "-":{
                result= calculator.getFirstNumber()-calculator.getSecondNumber();
                break;
            }
            default:{
                throw new UnsupportedOperationException("Wrong sign come");
            }
        }
        return result;
    }

}
